<?php
    $paramAccess = 'Please wait ... ';
    $paramContext = 'index';
?>
<!DOCTYPE html>
<html>
    <head>
        
        <title>WebTracker V2 :: ContentMedia/CrawlerIndex</title>
        
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/app.css" />
        
    </head>
    <body id="mainPage" class="page">
        
        <div class="container">
            <div class="content">
                
                <figure class="userForm">
                    
                    <header id="pageHeader" class="header">
                        <a href="/" title="WebTracker - Home" class="appBrand">
                            <h1 class="appName">
                                WebTracker v2
                            </h1>
                        </a>
                        <strong class="appSlogan">What does the Cloud know about you...?</strong>
                    </header>
                    
                    <section id="widgetArea" class="widget interact">
                        <form action="" method="get">
                            <input id="mainQuery" class="userInput" type="text" name="query" value="<?php
                                echo $paramAccess;
                            ;?>" placeholder="" autocomplete="off" />
                            <input type="hidden" name="on" value="<?php
                                echo $paramContext;
                            ;?>" />
                            <div class="queryBox">
                                <div class="left">
                                    <button class="userAct" type="submit" onclick="return false;">Process!</button>
                                </div>
                                <div class="right">
                                    <p class="match"></p>
                                </div>
                            </div>
                        </form>
                    </section>
                    
                    <footer id="pageFooter" class="footer">
                        <section class="infoTable">
                            <div class="dataFrame dataHeader">
                                <div class="id">Type: <span></span></div><br />
                                <div class="name">Name: <span></span></div><br />
                                <div class="status">Status: <span></span></div>
                            </div>
                            <div class="dataFrame dataScore">
                                <div class="rank">Rank: <span></span></div><br />
                                <div class="hits">Hits: <span></span></div>
                            </div>
                            <div class="dataFrame dataContext">
                                <div class="query">Query: <span></span></div><br />
                                <div class="response">Response: <span></span></div><br />
                                <div class="result">Result: <span></span></div>
                            </div>
                            <div class="dataFrame dataTimes">
                                <div class="crdate">Created: <span></span></div><br />
                                <div class="modded">Modified: <span></span></div>
                            </div>
                        </section>
                    </footer>
                    
                </figure>
                
            </div>
        </div>
        
        <script src="./../webtracker/node_modules/requirejs/require.js" data-main="./../webtracker/resources/assets/js/main"></script>
               
    </body>
</html>